﻿namespace projet_Csharp
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }



        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.deleteTaskBtn = new System.Windows.Forms.Button();
            this.DateDebPicker = new System.Windows.Forms.DateTimePicker();
            this.TaskName = new System.Windows.Forms.TextBox();
            this.DateFinPicker = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AddTaskBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TaskGrid = new System.Windows.Forms.DataGridView();
            this.TaskId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GridTaskName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateDeb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateFin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModifyTaskBtn = new System.Windows.Forms.Button();
            this.DetailTaskBtn = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaskGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // deleteTaskBtn
            // 
            this.deleteTaskBtn.Location = new System.Drawing.Point(593, 162);
            this.deleteTaskBtn.Name = "deleteTaskBtn";
            this.deleteTaskBtn.Size = new System.Drawing.Size(111, 36);
            this.deleteTaskBtn.TabIndex = 1;
            this.deleteTaskBtn.Text = "Supprimer une tache";
            this.deleteTaskBtn.UseVisualStyleBackColor = true;
            this.deleteTaskBtn.Click += new System.EventHandler(this.DeleteTaskBtn_Click);
            // 
            // DateDebPicker
            // 
            this.DateDebPicker.Location = new System.Drawing.Point(181, 52);
            this.DateDebPicker.Name = "DateDebPicker";
            this.DateDebPicker.Size = new System.Drawing.Size(200, 20);
            this.DateDebPicker.TabIndex = 2;
            // 
            // TaskName
            // 
            this.TaskName.Location = new System.Drawing.Point(12, 52);
            this.TaskName.Name = "TaskName";
            this.TaskName.Size = new System.Drawing.Size(163, 20);
            this.TaskName.TabIndex = 3;
            // 
            // DateFinPicker
            // 
            this.DateFinPicker.Location = new System.Drawing.Point(387, 52);
            this.DateFinPicker.Name = "DateFinPicker";
            this.DateFinPicker.Size = new System.Drawing.Size(200, 20);
            this.DateFinPicker.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(178, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Date de Début";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(384, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Date de Fin";
            // 
            // AddTaskBtn
            // 
            this.AddTaskBtn.Location = new System.Drawing.Point(593, 78);
            this.AddTaskBtn.Name = "AddTaskBtn";
            this.AddTaskBtn.Size = new System.Drawing.Size(111, 36);
            this.AddTaskBtn.TabIndex = 7;
            this.AddTaskBtn.Text = "Ajouter une tache";
            this.AddTaskBtn.UseVisualStyleBackColor = true;
            this.AddTaskBtn.Click += new System.EventHandler(this.AddTaskBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nom de la Tache";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(716, 24);
            this.menuStrip1.TabIndex = 10;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitterToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.QuitterToolStripMenuItem_Click);
            // 
            // TaskGrid
            // 
            this.TaskGrid.AllowUserToOrderColumns = true;
            this.TaskGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TaskGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TaskId,
            this.GridTaskName,
            this.DateDeb,
            this.DateFin});
            this.TaskGrid.Location = new System.Drawing.Point(12, 78);
            this.TaskGrid.Name = "TaskGrid";
            this.TaskGrid.Size = new System.Drawing.Size(575, 255);
            this.TaskGrid.TabIndex = 11;
            // 
            // TaskId
            // 
            this.TaskId.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.TaskId.HeaderText = "ID";
            this.TaskId.Name = "TaskId";
            this.TaskId.Width = 43;
            // 
            // GridTaskName
            // 
            this.GridTaskName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GridTaskName.HeaderText = "Nom";
            this.GridTaskName.Name = "GridTaskName";
            // 
            // DateDeb
            // 
            this.DateDeb.HeaderText = "Date de Début";
            this.DateDeb.Name = "DateDeb";
            this.DateDeb.Width = 150;
            // 
            // DateFin
            // 
            this.DateFin.HeaderText = "Date de Fin";
            this.DateFin.Name = "DateFin";
            this.DateFin.Width = 150;
            // 
            // ModifyTaskBtn
            // 
            this.ModifyTaskBtn.Location = new System.Drawing.Point(593, 120);
            this.ModifyTaskBtn.Name = "ModifyTaskBtn";
            this.ModifyTaskBtn.Size = new System.Drawing.Size(111, 36);
            this.ModifyTaskBtn.TabIndex = 8;
            this.ModifyTaskBtn.Text = "Modifier une tache";
            this.ModifyTaskBtn.UseVisualStyleBackColor = true;
            this.ModifyTaskBtn.Click += new System.EventHandler(this.ModifyTaskBtn_Click);
            // 
            // DetailTaskBtn
            // 
            this.DetailTaskBtn.Location = new System.Drawing.Point(593, 204);
            this.DetailTaskBtn.Name = "DetailTaskBtn";
            this.DetailTaskBtn.Size = new System.Drawing.Size(111, 36);
            this.DetailTaskBtn.TabIndex = 12;
            this.DetailTaskBtn.Text = "Afficher les détails";
            this.DetailTaskBtn.UseVisualStyleBackColor = true;
            this.DetailTaskBtn.Click += new System.EventHandler(this.DetailTaskBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 345);
            this.Controls.Add(this.DetailTaskBtn);
            this.Controls.Add(this.TaskGrid);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ModifyTaskBtn);
            this.Controls.Add(this.AddTaskBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DateFinPicker);
            this.Controls.Add(this.TaskName);
            this.Controls.Add(this.DateDebPicker);
            this.Controls.Add(this.deleteTaskBtn);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = " Gestionnaire de Tache";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing_1);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TaskGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button deleteTaskBtn;
        private System.Windows.Forms.DateTimePicker DateDebPicker;
        private System.Windows.Forms.TextBox TaskName;
        private System.Windows.Forms.DateTimePicker DateFinPicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button AddTaskBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.DataGridView TaskGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaskId;
        private System.Windows.Forms.DataGridViewTextBoxColumn GridTaskName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateDeb;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateFin;
        private System.Windows.Forms.Button ModifyTaskBtn;
        private System.Windows.Forms.Button DetailTaskBtn;
    }
}