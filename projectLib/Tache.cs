﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace projectLib
{
    public class Tache
    {
        protected int id;
        protected string name;
        protected DateTime dateDeb;
        protected DateTime dateFin;

        public Tache(int id, string name, DateTime dateDeb, DateTime dateFin)
        {
            this.id = id;
            this.name = name;
            this.dateDeb = dateDeb;
            this.dateFin = dateFin;
        }

        public void Modify(string name, DateTime dateDeb, DateTime dateFin)
        {
            this.name = name;
            this.dateDeb = dateDeb;
            this.dateFin = dateFin;
        }

        public TimeSpan Duree()
        {
            return this.dateFin - this.dateDeb ; //le jour de création de la tache est compter
        }

        public TimeSpan tpsRestant()
        {
            return this.dateFin - DateTime.Now; //le jour de création de la tache est compter
        }

        public String[] toRow()
        {
            string[] tmp = new string[4];
            tmp[0] = this.id.ToString();
            tmp[1] = this.name;
            tmp[2] = this.dateDeb.ToShortDateString();
            tmp[3] = this.dateFin.ToShortDateString();
            return tmp;
        }

        public override string ToString()
        {
            return string.Format("{0},{1},{2},{3}", this.id.ToString(), this.name, 
                this.dateDeb.ToString(), this.dateFin.ToString());
        }
        public int getId()
        {
            return this.id;
        }
        
        public string getName()
        {
            return this.name;
        }
        
        public DateTime getDateDeb()
        {
            return this.dateDeb;
        }

        public DateTime geDateFin()
        {
            return this.dateFin;
        }

        public void setId(int Id)
        {
            this.id = Id;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public void getDateDeb(DateTime dateDeb)
        {
            this.dateDeb = dateDeb;
        }

        public void getDateFin(DateTime dateFin)
        {
            this.dateFin = dateFin;
        }
    }
    public abstract class TaskList
    {
        protected static List<Tache> taskList = new List<Tache>();

        public static void add(Tache task)
        {
            if (taskList == null) taskList = new List<Tache>();
            taskList.Add(task);
        }

        public static void remove(Tache task)
        {
            if(taskList != null)
                taskList.Remove(task);
        }

        public static void removeAt(int id)
        {
            if(taskList != null)
            taskList.RemoveAt(id);
        }

        public static int maxId()
        {
            int id = 0;
            for(int i=0; i<taskList.Count; i++)
            {
                if (taskList[i].getId() > id) id = taskList[i].getId();
            }
            return id;
        }

        public static Tache getOne(int id)
        {
            if (taskList != null)
                return taskList[id];
            return null;
        }

        public static Tache getLast()
        {
            if (taskList != null)
                return taskList[taskList.Count-1];
            return null;
        }

        public static void save()
        {
            string path = @"tache.txt";
            using (StreamWriter file = new StreamWriter(path))
            {
                foreach (Tache elt in taskList)
                {
                    file.WriteLine(elt.ToString());
                }
            }
        }

        public static List<Tache> load()
        {
            string path = @"tache.txt";
            if(File.Exists(@"tache.txt") == false)
            {
                using (StreamWriter file = new StreamWriter(path))
                {
                    file.WriteLine("0,exemple,26/05/2042 00:00:00,27/05/2042 00:00:00");
                }
            }
            string[] taches = File.ReadAllLines(path);
            foreach(string elt in taches)
            {
                string[] tache = elt.Split(',');
                taskList.Add(new Tache(int.Parse(tache[0]), tache[1],
                    DateTime.Parse(tache[2]), DateTime.Parse(tache[3])));
            }
            return taskList;
        }
    }
}