# Gestionnaire de Taches

## UML

![Tache](_UML/TacheUML.png)
![TaskList](_UML/TaskListUML.png)

## Details
```
Ce projet est un gestionnaire de tache réalisé dans le cadre d'un devoir
Pour éviter d'éparpiller le code, les classes TaskList et Tache se partage la même fichier Tache.cs au sein de la projectLib
Le code permettant de gérer les évenements est dans le fichier prégéneré par VS
Projet à tester !! 
```
## Authors
Joséphine Herlemont et Quentin Champagne